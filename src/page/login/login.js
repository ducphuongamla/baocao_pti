import React from 'react';
import Login_form from '../../components/form/login/login_form';
import RegisterEmail_Form from '../../components/form/registerEmail/RegisterEmail_Form';
import OpenLockedAccount from '../../components/form/OpenLockedAccount/OpenLockedAccount';
import ResetPassword from '../../components/form/ResetPassword/ResetPassword';
import { useState } from 'react';

export default function Login() {

  const [open, setOpen] = useState(false);

  const [openLockedAccount, setOpenLockedAccount] = useState(false);

  const [openFormResetPass, setOpenFormResetPass] =useState(false)



  return (
    <>
      <Login_form
        setOpen={setOpen}
        setOpenLockedAccount={setOpenLockedAccount}
        setOpenFormResetPass={setOpenFormResetPass}
      />

      {open && <RegisterEmail_Form setOpen={setOpen} />}

      {openLockedAccount && (
        <OpenLockedAccount setOpenLockedAccount={setOpenLockedAccount} />
      )}

      {openFormResetPass && (
        <ResetPassword setOpenFormResetPass={setOpenFormResetPass} />
      )}
    </>
  );
}
