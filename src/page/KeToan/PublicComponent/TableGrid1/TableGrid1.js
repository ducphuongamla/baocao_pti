import React, { useState, useRef, useEffect } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TablePagination,
} from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";

const rowsPerPageOptions = [8, 10, 25, 35, 45, 55];

const TableGrid1 = (props) => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(rowsPerPageOptions[0]);
 
  const refs = useRef([]);
  const [checkedInput, setCheckedInput] = useState(null);
  const dispatch = useDispatch();

  const dataDefault = [
    { checkbox: "checkbox", id: "MA", TEN: "", k: "mdxc" },
    { checkbox: "checkbox", id: "MA", TEN: "", k: "mdxc" },
    { checkbox: "checkbox", id: "MA", TEN: "", k: "mdxc" },
    { checkbox: "checkbox", id: "MA", TEN: "", k: "mdxc" },
    { checkbox: "checkbox", id: "MA", TEN: "", k: "mdxc" },
    { checkbox: "checkbox", id: "MA", TEN: "", k: "mdxc" },
    { checkbox: "checkbox", id: "MA", TEN: "", k: "mdxc" },
    { checkbox: "checkbox", id: "MA", TEN: "", k: "mdxc" },
    { checkbox: "checkbox", id: "MA", TEN: "", k: "mdxc" },
    { checkbox: "checkbox", id: "MA", TEN: "", k: "mdxc" },
    { checkbox: "checkbox", id: "MA", TEN: "", k: "mdxc" },
  ];

 

  const columns = [
    { id: "MD", label: "Chọn xem" },
    { id: "MA", label: "ID" },
    { id: "TEN", label: "Tên báo cáo" },
  ];

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const start = page * rowsPerPage;
  const end = start + rowsPerPage;

  const paginatedData = dataDefault.slice(start, end);

  const handleOnchange = (e, index, das) => {
    const checked = e.target.checked;

    if (checked) {
      refs.current.forEach((input, pos) => {
        if (pos !== index && input) input.checked = false;
      });

     
    } else {
     
    }

    
  };

  let count = dataDefault.length;

  useEffect(() => {
    refs.current = [];
  }, [page]);

  return (
    <>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell key={column.id}>{column.label}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {paginatedData.map((row, index) => (
              <TableRow key={index}>
                {columns.map((column, columnIndex) => (
                  <TableCell key={`${column.id}-${columnIndex}`}>
                    {
                      <>
                        {row[column.id] === "BH" ? (
                          <input
                            type="checkbox"
                            ref={(element) => {
                              refs.current[index] = element;
                            }}
                            checked={checkedInput === row.MA}
                            onChange={(e) => handleOnchange(e, index, row)}
                          />
                        ) : (
                          row[column.id]
                        )}
                      </>
                    }
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={rowsPerPageOptions}
        component="div"
        count={count}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>
  );
};

export default TableGrid1;
