import React from "react";
import styles from "./styles.module.scss";
import TableGrid2 from "./TableGrid2/TableGid2";
import TableGrid1 from "./TableGrid1/TableGrid1";
import SelectData from "./Select/Select";
import Texarea from "../../../components/base/Texarea/Texarea";
import KeToanForm from "../../../components/form/ketoan/KeToanForm";

export default function PublicComponent(props) {
  const { title } = props;
 
  return (
    <div className={styles.public}>
      <div className="container">
        <br />
        <h2>{title}</h2>
        <br />

        <div className="row">
          <div className="col-7">
            <div className={styles.select}>
              <SelectData />
              <br />
              <TableGrid1 />
              <br />
            </div>
          </div>
          <div className="col-5">
            <div className={styles.grid2}>
              <TableGrid2 />
              <br />
              <Texarea />
              <KeToanForm />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
