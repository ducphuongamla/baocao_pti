import React from "react";
import PublicComponent from "../PublicComponent/PublicComponent";
import styles from "./styles.module.scss";

export default function TienTe() {
  const title = "Báo cáo bảo hiểm - Tiền tệ";

  return (
    <div className={styles.tiente}>
      <PublicComponent title={title} />
    </div>
  );
}
