import React from "react";
import PublicComponent from "../PublicComponent/PublicComponent";
import styles from './styles.module.scss';

export default function BaoHiem() {
    const title = 'Báo cáo bảo hiểm - Kế toán';

  return (
    <div className={styles.Baohiem}>
      <PublicComponent title={title}/>
    </div>
  );
}
