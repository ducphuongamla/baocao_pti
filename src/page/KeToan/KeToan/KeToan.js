import React from 'react';
import styles from './styles.module.scss';
import PublicComponent from '../PublicComponent/PublicComponent';

export default function KeToan() {
  const title = 'Báo cáo kế toán, tài chính';
  return (
    <div className={styles.Ketoan}>
       < PublicComponent title={title}/>
    </div>
  )
}
