import React from "react";
import PublicComponent from "../PublicComponent/PublicComponent";
import styles from "./styles.module.scss";

export default function VatTu() {
  const title = "Báo cáo bảo hiểm - Vật tư";

  return (
    <div className={styles.vattu}>
      <PublicComponent title={title} />
    </div>
  );
}
