import React from "react";
import PublicComponent from "../PublicComponent/PublicComponent";
import styles from "./styles.module.scss";

export default function ThueVAT() {
  const title = "Báo cáo nghiệp vụ kế toán - Thuế VAT";

  return (
    <div className={styles.thue}>
      <PublicComponent title={title} />
    </div>
  );
}
