import React from "react";
import PublicComponent from "../PublicComponent/PublicComponent";
import styles from "./styles.module.scss";

export default function PhanBo() {
  const title = "Báo cáo bảo hiểm - Phân bổ";

  return (
    <div className={styles.PhanBo}>
      <PublicComponent title={title} />
    </div>
  );
}
