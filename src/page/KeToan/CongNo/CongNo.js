import React from 'react';
import PublicComponent from '../PublicComponent/PublicComponent';
import styles from './styles.module.scss'

export default function CongNo() {
    const title = "Báo cáo nghiệp vụ kế toán - Công nợ";

  return (
    <div className={styles.Congno}>
        <PublicComponent title={title}/>
    </div>
  )
}
