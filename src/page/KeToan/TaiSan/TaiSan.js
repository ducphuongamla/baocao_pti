import React from "react";
import PublicComponent from "../PublicComponent/PublicComponent";
import styles from "./styles.module.scss";

export default function TaiSan() {
  const title = "Báo cáo bảo hiểm - Tài sản";

  return (
    <div className={styles.TaiSan}>
      <PublicComponent title={title} />
    </div>
  );
}
