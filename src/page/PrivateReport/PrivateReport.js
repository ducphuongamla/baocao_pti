import React, { useEffect, useState} from "react";
import TableLeft from "./TableLeft/TableLeft";
import TableRight from "./TableRight/TableRight";
import Texarea from "../../components/base/Texarea/Texarea";
import { Button } from "@material-ui/core";
import PrivateReportForm from "../../components/form/PrivateReportForm/PrivateReportForm";
import { get_api_init } from "../../slice/reportPrivateSlice";
import { unwrapResult } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import SearchBar from "../../components/base/InputSearch/InputSearch";
import BasicSelect from "./Select/Select";



let isInit = false;

export default function PrivateReport() {

  const dispatch = useDispatch();

  const  [datas, setDatas] = useState({
    name_BC: '',

  })


  const callDataInit = async (data) => {
      try {

          // const value = {
          //   cot: "ten,ma,ddan,rep",
          //   data: JSON.stringify({
          //     menu: 'BHBC_KHTC',
          //     nhom: 'BHHHBH',
          //     loai: 'D',
          //     tim: '',
          //     sl: 'C',
          //     tu: 1,
          //     den: 15
          //   }),
          // };

          const action = get_api_init(data);
          const res = await dispatch(action);
          const result = unwrapResult(res);

          if(result.code === '000') {

            console.log(result)
          }



      } catch {
        console.log('erorr:')
      }
  }



  useEffect(() => {
    if(!isInit){
      isInit = true;
      callDataInit({data: JSON.stringify({ md: 'BCHHBH'})})

    }
  },[])


  return (
    <div>
      <div className="container">
        <br/>
        <h2>Báo cáo Hiệp hội bảo hiểm</h2>
        <br/>
        <p>Loại báo cáo</p>

        <div className="row">
          <div className="col-7">
            <BasicSelect />
            <br />
            <SearchBar />
            <br />
            <TableLeft />
          </div>

          <div className="col-5">
            <div className="row">
              <div className="col-12">
                <TableRight />
              </div>
              <br />
              <div className="col-12">
                <Texarea />

                <PrivateReportForm />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
