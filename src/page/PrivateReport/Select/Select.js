import * as React from "react";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { useEffect } from "react";
import { useDispatch } from "react-redux";

const BasicSelect = (props) => {
  const [age, setAge] = React.useState("");

  const dispatch = useDispatch();



  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const handleRequest = (value) => {
    {

    }
  };

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Nhóm</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={age}
          label="Age"
          onChange={handleChange}
        ></Select>
      </FormControl>
    </Box>
  );
};

export default BasicSelect;
