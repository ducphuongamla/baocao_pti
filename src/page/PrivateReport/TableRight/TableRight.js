import React, { useEffect, useRef } from "react";
import { makeStyles } from "@mui/styles";
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper,
} from "@mui/material";

import { useSelector, useDispatch } from "react-redux";

const useStyles = makeStyles({
  root: {
    width: "100%",
    maxHeight: 200,
    overflowY: "scroll",
    "&::-webkit-scrollbar": {
      width: "6px",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#888",
      borderRadius: "3px",
    },
    "&::-webkit-scrollbar-track": {
      backgroundColor: "#f1f1f1",
    },
  },
  tableHead: {
    position: "sticky",
    top: 0,
    backgroundColor: "#fff",
    zIndex: 1,
  },
  tableCell: {
    fontWeight: "bold",
  },
});

const TableRight = (props) => {
  const classes = useStyles();
  
  const inputRefs = useRef([]);

  const rows = [{ cell3: "" }, { cell3: "" }, { cell3: "" }];
  const data = [];

  const rowpage = rows;

  const handleOnchange = (e, value, index) => {
    const checked = e.target.checked;

    if (checked) {
      

      inputRefs.current.forEach((input, pos) => {
        if (pos !== index && input) {
          input.checked = false;
        }
      });
    }
  };

  useEffect(() => {
    // set checked value to false for all input elements
    inputRefs.current.forEach((input) => {
      if (input && input.checked) {
        input.checked = false;
      }
    });
  }, [data]);

  return (
    <TableContainer component={Paper} className={classes.root}>
      <Table>
        <TableHead className={classes.tableHead}>
          <TableRow>
            <TableCell className={classes.tableCell}>Chọn xem</TableCell>
            <TableCell className={classes.tableCell}>Mẫu báo cáo</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rowpage.map((row, index) => (
            <TableRow key={index}>
              <TableCell>
                {data && data.length > 0 ? (
                  <input
                    type={"checkbox"}
                    ref={(Element) => {
                      inputRefs.current[index] = Element;
                    }}
                    onChange={(e) => handleOnchange(e, row, index)}
                  />
                ) : (
                  <></>
                )}
              </TableCell>

              <TableCell>{row.TEN}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default TableRight;
