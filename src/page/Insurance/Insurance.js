import React, { useState } from "react";
import { useEffect } from "react";
import { unwrapResult } from "@reduxjs/toolkit";
import { useDispatch, useSelector } from "react-redux";

import Texarea from "../../components/base/Texarea/Texarea";
import styles from "./styles.module.scss";
import PaginatedTable from "../../components/base/Table/TablePagination/TablePagination";
import MyTable from "../../components/base/Table/TableGrid2/TableGrid2.js";
import BasicSelect from "../../components/base/Select/Select";
import SearchBar from "../../components/base/InputSearch/InputSearch";
import CustomizedDialogs from "../../components/form/insurance/FormReview";

import {
  InitView,
  ListSubGroup,
  ListCode,
  ListCodeTemplate,
  RenderController,
} from "../../slice/insuranceSlice";

import {
  visible,
  hidden,
  DataKD,
  DataTableLeft,
  DataTableRight,
  requestController,
  requestDrop,
  printBC,
  nhomBC,
  TenRP,
  labelController,
  resetDataTableRight,
} from "../../slice/insuranceSlice";

var isInitView = false;

export default function Insurance() {
  const dataKD = useSelector((state) => state.insurance.dataKD);
  const dataTableLeft = useSelector((state) => state.insurance.dataTableLeft);

  const dispatch = useDispatch();

  const inittial = { title: "Báo cáo bảo hiểm", md: "BH" };

  const [data, setData] = useState({
    nhom: [],
    pnhom: [],
    nhomSelect: null,
    pnhomSelect: null,

    objectNhomSelect: {
      MA: "",
      TEN: "",
    },
    objectNhomSelect2: {
      MA: "",
      TEN: "",
    },
  });

  // Gọi api khi mới vào

  const requestInitView = async (values) => {
    try {
      const action = InitView(values);
      const resultAction = await dispatch(action);
      const response = unwrapResult(resultAction);
      if (response.code === "000") {
        setData({
          ...data,
          nhom: response.data.nhom,
        });

       // console.log("initview", response);
        dispatch(printBC(response.data.kieu_in));
        dispatch(DataKD(response.data.nhom));
      } else {
        alert(response.message);
      }
    } catch (error) {
      console.log("error: ", error);
    }
  };

  // lấy api phân nhóm
  const requestListSubGroup = async (value) => {
    try {
      const values = {
        data: JSON.stringify({ md: "BH", nhom: value, sl: "D" }),
      };
      const resultAction = await dispatch(ListSubGroup(values));

      const response = unwrapResult(resultAction);
      if (response.code === "000") {
        if (dataKD) {
          setData({
            ...data,
            nhom: dataKD,
            nhomSelect: value,
            pnhom: response.data.pnhom,
            objectNhomSelect: dataKD[0],
            objectNhomSelect2: response.data.pnhom[0],
          });

         // console.log("value", value);
        }
      } else {
        alert(response.message);
      }
    } catch (error) {
      console.log("error: ", error);
    }
  };

  // Lấy api tablelef
  const requestListCode = async (value) => {
    try {
      const values = {
        data: JSON.stringify({ md: "BH", nhom: data.nhomSelect, pnhom: value }),
      };
      dispatch(nhomBC(data.nhomSelect));

      const action = ListCode(values);
      const resultAction = await dispatch(action);
      const response = unwrapResult(resultAction);

      if (response.code === "000") {
        //console.log("listcode:", response);
      } else {
        alert(response.message);
      }
    } catch (error) {
      console.log("error: ", error);
    }
  };

  // gọi api table right
  const requestListCodeTemplate = async (value) => {
    try {
      console.log("value .MA", value.MA);
      const action = ListCodeTemplate({
        data: JSON.stringify({
          ma: value.MA,
          ten: value.MA,
          dong: "8",
          sl: "D",
        }),
      });
      const resultAction = await dispatch(action);
      const response = unwrapResult(resultAction);

      if (response.code === "000") {

      } else {
        alert(response.message);
      }
    } catch (error) {
      console.log("error: ", error);
    }
  };


  // lấy api cho form control
  const RequestViewFormController = async (value) => {
    console.log(value);
    try {
      dispatch(labelController(value.TEN));
      dispatch(TenRP(value.TEN_RP));
      const action = RenderController({
        data: JSON.stringify({ ma_bc: value.MA, ten: value.TEN_RP }),
      });
      const resultAction = await dispatch(action);
      const response = unwrapResult(resultAction);

      if (response.code === "000") {
        console.log("RequestController:", response);
        dispatch(requestController(response.data.ds_dk));
        dispatch(requestDrop(response.data.ds_dk_lst));
      } else {
        alert(response.message);
      }
    } catch (error) {
      console.log("error: ", error);
    }
  };





  useEffect(() => {
    if (data.nhomSelect !== null || data.nhomSelect !== undefined) {
      requestListCode("");
    }
  }, [data.nhomSelect]);

  useEffect(() => {
    if (dataKD) {
      requestListSubGroup("");
    }
  }, [dataKD]);

  useEffect(() => {
    if (!isInitView) {
      isInitView = true;
      requestInitView({ data: JSON.stringify({ md: inittial.md }) });
    }
  }, []);


  return (
    <div className={styles.insurance}>
      <div className="container">
        <h3>Báo cáo nghiệp vụ bảo hiểm</h3>
        <div className="row">
          <div className="col-sm-12 col-lg-7">
            <div className={styles.select}>
              <span>Liệt kê</span>
              <br />
              <BasicSelect
                data={data}
                type={"nhom"}
                requestListSubGroup={requestListSubGroup}
                resetDataTableRight={resetDataTableRight}
              />
              <br />
              <BasicSelect
                data={data}
                type={"phannhom"}
                requestListCode={requestListCode}
                resetDataTableRight={resetDataTableRight}
              />
            </div>
              <br />
            <PaginatedTable
              dataTableLeft={dataTableLeft}
              requestListCodeTemplate={requestListCodeTemplate}
              resetDataTableRight={resetDataTableRight}
            />
            <br />
          </div>

          <div className="col-sm-12 col-lg-5">
            <div className={styles.rowright}>
              <div>
                <div className={styles.search}>
                  <span>Tìm BC</span>
                  <br />
                  <SearchBar />
                </div>
                <br />

                <MyTable
                  RequestViewFormController={RequestViewFormController}
                />
              </div>
              <div className="mt-5">
                <Texarea />
              </div>
              <CustomizedDialogs />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
