import { useEffect, useLayoutEffect, useMemo } from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import * as path from "./constant/routers";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.scss";
import Login from "./page/Login/login";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import Insurance from "./page/Insurance/Insurance";
import PrivateReport from "./page/PrivateReport/PrivateReport";
import BaoHiem from "./page/KeToan/BaoHiem/BaoHiem";
import CongNo from "./page/KeToan/CongNo/CongNo";
import PhanBo from "./page/KeToan/PhanBo/PhanBo";
import TaiSan from "./page/KeToan/TaiSan/TaiSan";
import ThueVAT from "./page/KeToan/ThueVAT/ThueVAT";
import TienTe from "./page/KeToan/TienTe/TienTe";
import VatTu from "./page/KeToan/VatTu/VatTu";
import KeToan from "./page/KeToan/KeToan/KeToan.js";



function App() {
  const token = localStorage.getItem("token");

  useEffect(() => {
    if (!token) {
      <Navigate to={`${path.login}`} replace={true} />;
    } 
  }, [token]);

  return (
    <div>
      {!token ? (
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path={`${path.login}`} element={<Login />} />
        </Routes>
      ) : (
        <div>
          <Header />
          <Routes>
            <Route path={`${path.insurance}`} element={<Insurance />} />
            <Route path={`${path.PrivateReport}`} element={<PrivateReport />} />

            <Route path={`${path.ketoan.ketoan}`} element={<KeToan />} />
            <Route path={`${path.ketoan.baohiem}`} element={<BaoHiem />} />
            <Route path={`${path.ketoan.congno}`} element={<CongNo />} />
            <Route path={`${path.ketoan.thueVAT}`} element={<ThueVAT />} />
            <Route path={`${path.ketoan.taisan}`} element={<TaiSan />} />
            <Route path={`${path.ketoan.tiente}`} element={<TienTe />} />
            <Route path={`${path.ketoan.vattu}`} element={<VatTu />} />
            <Route path={`${path.ketoan.phanbo}`} element={<PhanBo />} />

            <Route />
          </Routes>
          {/* <Footer /> */}
        </div>
      )}
    </div>
  );
}

export default App;
