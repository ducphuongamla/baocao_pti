import * as React from "react";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { useEffect } from "react";
import { useDispatch } from "react-redux";

const BasicSelect = (props) => {
  const [age, setAge] = React.useState("");
  const {
    data,
    type,
    requestListSubGroup,
    requestListCode,
    resetDataTableRight,
  } = props;

  const dispatch = useDispatch();

  const renderProps = () => {
    switch (type) {
      case "nhom":
        return data.nhom.map((item, index) => (
          <MenuItem
            value={item.TEN}
            key={index}
            onClick={(e) => handleRequest(item)}
          >
            {item.TEN}
          </MenuItem>
        ));

      case "phannhom":
        return data.pnhom.map((item) => (
          <MenuItem
            value={item.TEN}
            key={item.TEN}
            onClick={() => {
              dispatch(resetDataTableRight());
              requestListCode(item.MA);
            }}
          >
            {item.TEN}
          </MenuItem>
        ));

      default:
        return;
    }
  };

  const inputLabel = () => {
    switch (type) {
      case "nhom":
        return <InputLabel id="demo-simple-select-label">Nhóm</InputLabel>;

      case "phannhom":
        return <InputLabel id="demo-simple-select-label">Phân nhóm</InputLabel>;

      default:
        return;
    }
  };

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const handleRequest = (value) => {
    {
      dispatch(resetDataTableRight());

      requestListSubGroup(value.MA);
    }
  };

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        {inputLabel()}
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={age}
          label="Age"
          onChange={handleChange}
        >
          {renderProps()}
        </Select>
      </FormControl>
    </Box>
  );
};

export default BasicSelect;
