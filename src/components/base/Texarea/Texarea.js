import React from "react";
import styles from "./styles.module.scss";

export default function Texarea(props) {
  const { content } = props;

  const data = content ? content : 'Miêu tả';


  return (
    <div className={styles.text}>
      <h4 className={styles.title}>Miêu tả</h4>
      <div className={styles.textarea}>
        <textarea defaultValue={data}></textarea>
      </div>
      <div className={styles.btn}></div>
    </div>
  );
}
