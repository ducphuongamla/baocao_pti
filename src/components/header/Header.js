import React, { useEffect, useState } from "react";
import styles from "./styles.module.scss";
import logo from "../../assets/logo.png";
import * as path from '../../constant/routers';
import { NavLink } from "react-router-dom";


export default function Header() {

  const [prevMenuItem, setPrevMenuItem] = useState(null);
  const [prevSelect, setPrevSelect] = useState(null);

  const handleClick = (e) => {
    const current = e.target;
    let menuItem = e?.target?.parentNode;
    while (!menuItem?.classList?.contains("menuItem") && menuItem) {
      menuItem = menuItem?.parentNode;
    }
    const blockUL = e.target.nextElementSibling;

    // Nếu cùng khối
    if (prevMenuItem?.innerText === menuItem?.innerText) {
      blockUL?.classList?.toggle("open");

      if (!blockUL?.classList?.contains("open")) {
        blockUL?.querySelectorAll(".open").forEach((ul) => {
          ul.classList.remove("open");
        });
      }
    } else {
      setPrevMenuItem(menuItem);
      if (prevMenuItem) {
        prevMenuItem
          .querySelectorAll(".open")
          .forEach((ul) => ul.classList.remove("open"));

        prevMenuItem.querySelectorAll(".bgr").forEach((a) => {
          a.classList.remove("bgr");
        });
      }
      blockUL?.classList?.toggle("open");
    }

    // set color bacgorund
    if (e.target.parentNode.classList.contains("menuItem")) {
      if (prevSelect?.innerText === e.target?.innerText) {
        e.target.classList.toggle("select");
      } else {
        setPrevSelect(e.target);
        if (prevSelect) {
          prevSelect.classList.remove("select");
        }
        e.target.classList.toggle("select");
      }
    }

    // set background item
    if (!e.target.parentNode.classList.contains("menuItem") && blockUL) {
      if (
        prevMenuItem?.innerText === menuItem?.innerText &&
        menuItem.childNodes[0].classList.contains("select")
      ) {
        current.classList.toggle("bgr");
      } else {
        blockUL
          .querySelectorAll(".bgr")
          .forEach((a) => a.classList.remove("bgr"));
      }
    } else {
      if (blockUL) {
        blockUL
          .querySelectorAll(".bgr")
          .forEach((a) => a.classList.remove("bgr"));
      }
    }

    if (blockUL && !current.classList.contains("bgr")) {
      blockUL
        .querySelectorAll(".bgr")
        .forEach((a) => a.classList.remove("bgr"));
    }
  };


  const handleLogout = () => {
   
    localStorage.removeItem('token');
    window.location.href = `${path.login}`;
  }


  useEffect(() => {
    const handleHideMenu = (event) => {
      const menu = document.querySelector(".menu");
      const isClickInsideMenu = menu?.contains(event.target);

      if (!isClickInsideMenu && prevMenuItem) {
        prevMenuItem
          .querySelectorAll(".open")
          .forEach((i) => i.classList.remove("open"));
        prevMenuItem
          .querySelectorAll(".select")
          .forEach((i) => i.classList.remove("select"));
        prevMenuItem
          .querySelectorAll(".bgr")
          .forEach((i) => i.classList.remove("bgr"));
      }
    };

    document.addEventListener("mousedown", handleHideMenu);
  });

  return (
    <div className={styles.header}>
      <div className="container">
        <div className={styles.wrapp}>
          <div className={styles.logo}>
            <img src={logo}></img>
          </div>
          <ul className={`${styles.menu} menu`} onClick={handleClick}>
            <li className={`${styles.menuItem} menuItem`}>
              <a>
                Kế toán <b className="caret"></b>
              </a>
              <ul>
                <li>
                  <a href={`${path.ketoan.ketoan}`}>Kế toán</a>
                </li>
                <li>
                  <a href={`${path.ketoan.vattu}`}>Vật tư</a>
                </li>
                <li>
                  <a href={`${path.ketoan.congno}`}>Công nợ</a>
                </li>
                <li>
                  <a href={`${path.ketoan.tiente}`}>Tiền tệ</a>
                </li>
                <li>
                  <a href={`${path.ketoan.thueVAT}`}>Thuế VAT</a>
                </li>
                <li>
                  <a href={`${path.ketoan.taisan}`}>Tài sản</a>
                </li>
                <li>
                  <a href={`${path.ketoan.phanbo}`}>Phân bổ</a>
                </li>
                <li>
                  <a href={`${path.ketoan.baohiem}`}>Bảo hiểm</a>
                </li>
              </ul>
            </li>
            <li className={`${styles.menuItem}`}>
              <a href={`${path.insurance}`}>Bảo hiểm</a>
            </li>
            <li className={`${styles.menuItem} menuItem`}>
              <a>
                Báo cáo riêng <b className="caret"></b>
              </a>
              <ul>
                <li>
                  <a href={`${path.PrivateReport}`}>
                    Báo cáo hiệp hội bảo hiểm
                  </a>
                </li>
              </ul>
            </li>
            <li className={`${styles.menuItem} menuItem`}>
              <a>
                Tra cứu <b className="caret"></b>
              </a>
              <ul>
                <li>
                  <a href={`${path.Tracuu.Ks_TLBT}`}>
                    Kiểm soát tỷ lệ bồi thường
                  </a>
                </li>
                <li>
                  <a>Hóa đơn ấn chỉ</a>
                </li>
                <li>
                  <a>
                    Hệ thống KPI <b className="caret"></b>
                  </a>
                  <ul>
                    <li>
                      <a>Số liệu KPI</a>
                    </li>
                    <li>
                      <a>KPI Bồi thường viên</a>
                    </li>
                    <li>
                      <a>Tham số KPI</a>
                    </li>
                    <li>
                      <a>Tổng hợp KPI Bồi thường viên</a>
                    </li>
                    <li>
                      <a>Tổng hợp KPI</a>
                    </li>
                    <li>
                      <a>Tổng hợp KPI 2020</a>
                    </li>
                    <li>
                      <a>Số liệu KPI 2020</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li className={`${styles.menuItem} menuItem`}>
              <a>
                <b className="fa fa-cog"></b>
              </a>
              <ul>
                <li>
                  <a>Xin chào: Nhóm kế toán tổng hợp VP TCty</a>
                </li>

                <li>
                  <a>Đổi mật khẩu</a>
                </li>

                <li>
                  <NavLink to={`${path.login}`} onClick={handleLogout}>
                    Đăng nhập lại
                  </NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}



// const menu = [
//   {
//      name: 'Kế toán',
//      childrens: [{name: }]
//   }, 

//   {
//     name: 'Bảo hiểm',
//     childrens: []
//   },

//   {
//     name: 'Báo cáo riêng',
//     childrens: []
//   },
  
//   {
//     name: 'Tra cứu',
//     childrens: []
//   },

//   {
//     name: 'setting',
//     childrens: []
//   }


// ]