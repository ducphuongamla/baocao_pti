import React from "react";
import styles from "./styles.module.scss";

export default function Footer() {
  return (
    <div className={styles.footer}>
      <div className="container">
        <p>
          Báo cáo Nghiệp vụ là một sản phẩm của Ban Công nghệ Thông tin - Tổng
          công ty CP Bảo Hiểm Bưu Điện (PTI).
        </p>
        <p>
          © 2014-2016 Information Technology Division.Post & Telecomunication
          Insurance Corporation. All rights reserved.
        </p>
      </div>
    </div>
  );
}
