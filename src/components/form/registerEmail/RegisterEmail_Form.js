import * as React from "react";
import styles from "./styles.module.scss";

export default function RegisterEmail_Form({ setOpen }) {
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className={styles.wrapper}>
      <div className="container">
        <div className={styles.form}>
          <form>
            <div className={styles.title}>
              <p>
                Cập nhập email cho NSD
                <span> (Lưu ý: Chỉ đăng ký 1 lần để truy cập ứng dụng)</span>
              </p>
              <span
                onClick={handleClose}
                style={{
                  cursor: "pointer",
                  transform: "translateY(1rem) scale(1.5)",
                  color: "#333",
                  fontWeight: "600",
                }}
              >
                X
              </span>
            </div>

            <div className={styles.content}>
              <div className={styles.block}>
                <div className={styles.item}>
                  <label>Đơn vị</label>
                  <select placeholder="Tổng công ty Cổ phần Bảo hiểm Bưu điện"></select>
                </div>

                <div className={styles.item}>
                  <label>Mã NSD</label>
                  <input placeholder="MÃ NGƯỜI SỬ DỤNG" />
                </div>

                <div className={styles.item}>
                  <label>Mật khẩu</label>
                  <input placeholder="password" />
                </div>

                <div className={styles.item}>
                  <label>Email</label>
                  <input placeholder="Email, ví dụ: *****@pti.com.vn" />
                </div>

                <div className={styles.item}>
                  <label>Phone</label>
                  <input placeholder="Số điện thoại" />
                </div>
              </div>

              <h3>Cách thức để khai báo email cho Người sử dụng</h3>
              <ul>
                <li>Chọn đơn vị bạn đang sử dụng</li>
                <li>Gõ Mã người sử dụng đang sử dụng</li>
                <li>Gõ đúng mật khẩu của Mã người sử dụng</li>
                <li>
                  Bấm{" "}
                  <span style={{ color: "#ff0000", fontWeight: 600 }}>
                    Đăng ký Email
                  </span>
                </li>
                <li>
                  <span style={{ fontWeight: 700 }}>
                    Tại sao bạn nên đăng ký email, điện thoại cho Người sử dụng
                    phần mềm nghiệp vụ
                  </span>
                  <ul>
                    <li>
                      1. Để đăng nhập vào hệ thống báo cáo, Cổng thông tin, nhập
                      liệu chung của tổng công ty.
                    </li>
                    <li>
                      2. Sử dụng email để mở khóa, lấy lại mật khẩu của phần mềm
                      nghiệp vụ.
                    </li>
                    <li>
                      3. Các hệ thống cảnh báo, email tiện ích sử dụng do Ban
                      CNTT phát triển tiếp sau.
                    </li>
                  </ul>
                </li>
              </ul>
              <p style={{ fontWeight: 700 }}>
                Và còn nhiều tiện ích khác liên quan theo định hướng của Ban
                Công nghệ thông tin.
              </p>
            </div>

            <div className={styles.footer}>
              <button>đăng ký email</button>
              <button>hủy đăng ký email</button>
              <div onClick={handleClose}>close</div>
            </div>
          </form>
        </div>
      </div>
      <div className={styles.cover} onClick={handleClose}></div>
    </div>
  );
}
