import * as React from "react";
import debounce from "../../base/debounce/debounce";
import * as status from "../../../constant/status_code";
import { useDispatch, useSelector } from "react-redux";
import {
  check_email,
  check_select,
  check_login,
} from "../../../slice/loginSlice";
import { unwrapResult } from "@reduxjs/toolkit";
import { useState, useEffect } from "react";
import styles from "./styles.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { Route, Routes, Navigate } from "react-router-dom";

export default function Login_form({
  setOpen,
  setOpenLockedAccount,
  setOpenFormResetPass,
}) {
  const dispatch = useDispatch();
  const login = useSelector((state) => state.login.current);
  const [prevEmail, setPrevEmail] = useState("");
  const [email, setEmail] = useState("");

  const ma_nsd = login?.data?.ds_nsd || [];
  const donvi = login?.data?.ds_dns || [];

  const [ma_dns, setMa_dns] = useState(null);

  const handleCheckEmail = async (e) => {
    e.preventDefault();
    setEmail(e.target.value);
    if (e.target.value !== prevEmail && e.target.value.trim() !== "") {
      const data = await dispatch(check_email(e.target.value));
      const result = unwrapResult(data);
      console.log(result);

      if (result.code === status.ERRROR) {
        alert("fail");
      } else {
        // setPrevEmail(email);
        setMa_dns(result.data.ds_dns[0].DNS);

        const res = await dispatch(
          check_select({
            dns: result.data.ds_dns[0].DNS,
            email: e.target.value,
          })
        );

        if (res.code === status.SUCCESS) {
          const token = JSON.stringify(res.data);
          localStorage.setItem(token);
        }
      }
    }
  };

  const debounceHandleCheckEmail = debounce((e) => {
    handleCheckEmail(e);
  });

  const change = (e) => {
    const dns = [...donvi].find((i) => i.TEN === e.target.value).DNS;
    setMa_dns(dns);

    dispatch(check_select({ dns, email }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const email = e.target.querySelector("[name=email]").value;
    const ma_nsd = e.target.querySelector("[name=nsd]").value;
    const password = e.target.querySelector("[name=password").value;

    const data = {
      email: email,
      maDonvi: ma_dns,
      maNsd: ma_nsd,
      password: password,
    };

    if (password.trim() !== "" && email.trim() !== "") {
      const respone = await dispatch(check_login(data));
      const result = unwrapResult(respone);

      console.log(result);

      if (result.code === status.SUCCESS) {
        const token = result.data;
        localStorage.setItem("token", token);

        window.location.href =
          "http://localhost:3000/B%E1%BA%A3o-hi%E1%BB%83m/B%E1%BA%A3o-hi%E1%BB%83m.htm";
        alert("Đăng nhập thành công");
      } else {
        alert(result.message);
      }
    }
  };

  return (
    <div style={{ width: "100%" }}>
      <div className="container" style={{ paddingTop: "30px" }}>
        <div className={styles.loginForm}>
          <div className={styles.title}>
            <h1>Đăng nhập</h1>
          </div>

          <div className="row" style={{ padding: "20px" }}>
            <div className="col-sm-12 col-md-12 col-lg-6 pt-4">
              <form onSubmit={handleSubmit}>
                <div className="email">
                  <label>Email</label>
                  <input
                    placeholder="Email, ví dụ: *****@pti.com.vn"
                    style={{ width: "100%" }}
                    type="text"
                    onChange={debounceHandleCheckEmail}
                    name="email"
                    defaultValue={email}
                    onBlur={(e) => setEmail(e.target.value)}
                  ></input>
                </div>

                <div className="donvi">
                  <label>Đơn vị</label>
                  <select
                    style={{ width: "100%" }}
                    onChange={change}
                    name="donvi"
                  >
                    {donvi.length > 0 ? (
                      donvi.map((i) => (
                        <option key={i.TEN} onClick={() => setMa_dns(i.DNS)}>
                          {i.TEN}
                        </option>
                      ))
                    ) : (
                      <option></option>
                    )}
                  </select>
                </div>

                <div className="nsd">
                  <label>Mã NSD</label>
                  <select style={{ width: "100%" }} name="nsd">
                    {ma_nsd ? (
                      ma_nsd.map((i) => <option key={i}>{i.MA}</option>)
                    ) : (
                      <option></option>
                    )}
                  </select>
                </div>

                <div className="password">
                  <label style={{opacity: ma_nsd.length > 0 && donvi.length > 0 ? '100' : 0}}>
                    Mật khẩu của: <span style={{ color: "red" }}>Email</span>
                  </label>

                  <input id="pass" style={{ width: "100%" }} name="password" type='password'/>
                </div>

                <div>
                  <input type="checkbox" />
                  <label>Chạy tài khoản báo cáo tự động: BCTD</label>
                </div>

                <div>
                  <input type="checkbox" />
                  <label>Nhận số liệu qua email khi chạy báo cáo</label>
                </div>

                <button onClick={() => handleSubmit}>đăng nhập</button>

                <a href="#">xem hướng dẫn sử dụng báo cáo</a>
              </form>
            </div>

            <div className="col-sm-12 col-md-12 col-lg-6">
              <div className={styles.systemReport}>
                <h2>hệ thống báo cáo nghiệp vụ</h2>
                <ul>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheck}
                      style={{ color: "green" }}
                    />
                    Ứng dụng xem báo cáo chi tiết phân theo từng nghiệp vụ
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheck}
                      style={{ color: "green" }}
                    />
                    Ứng dụng xem báo cáo nhanh, chính xác, kịp thời ở mọi thời
                    điểm
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheck}
                      style={{ color: "green" }}
                    />
                    Ứng dụng không cần cài đặt
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheck}
                      style={{ color: "green" }}
                    />
                    Ứng dụng sử dụng mọi lúc mọi nơi
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheck}
                      style={{ color: "green" }}
                    />
                    Ứng dụng hỗ trợ kết xuất nhiều định dạng báo cáo
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheck}
                      style={{ color: "green" }}
                    />
                    Sản phẩm của ban CNTT, Tổng công ty CP bảo hiểm PTI
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheck}
                      style={{ color: "green" }}
                    />
                    Mọi thắc mắc liên hệ: cntt@pti.com.vn
                  </li>
                  <li>
                    <FontAwesomeIcon
                      icon={faCheck}
                      style={{ color: "green" }}
                    />
                    Không hỗ trợ sử dụng trình duyệt Cốc Cốc
                  </li>
                </ul>

                <button
                  className={styles.btnSignIn}
                  onClick={() => setOpen(true)}
                >
                  Đăng ký Email cho Người sử dụng Phần mềm nghiệp vụ
                </button>

                <button
                  className={styles.openAccount}
                  onClick={() => setOpenLockedAccount(true)}
                >
                  <span>Mở tài khoản bị khóa Phần mềm nghiệp vụ</span>
                </button>

                <button
                  className={styles.btnReset}
                  onClick={() => setOpenFormResetPass(true)}
                >
                  <span>Click Reset mật khẩu tài khoản phần mềm nghiệp vụ</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
