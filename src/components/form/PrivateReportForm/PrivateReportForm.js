import * as React from "react";
import PropTypes from "prop-types";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import DatePickerValue from "../../base/DatePicker/DatePicker";
import Input from "../../base/Input/Input";
import BasicSelect from "../../base/Select/Select";


const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

function BootstrapDialogTitle(props) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other} style={{ minWidth: "60rem" }}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

const PrivateReportForm = () => {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="outlined" onClick={handleClickOpen}>
        Chọn Xem
      </Button>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Modal title
        </BootstrapDialogTitle>
        <DialogContent dividers>
          <form style={{ padding: "0 2rem" }}>
            <div>
              <span>Loại báo cáo</span>
              <BasicSelect />
            </div>

            <div>
              <span>Từ ngày</span>
              <DatePickerValue />
            </div>

            <div>
              <span>Đến ngày</span>
              <DatePickerValue />
            </div>

            <div>
              <span>Kiểu KT</span>
              <BasicSelect />
            </div>

            <div>
              <span>Lấy KHTN</span>
              <BasicSelect />
            </div>

            <div>
              <span>Kiểu lấy số liệu</span>
              <BasicSelect />
            </div>

            <div>
              <span>Mẫu BC</span>
              <BasicSelect />
            </div>

            <div>
              <span>Kiểu xem</span>
              <BasicSelect />
            </div>
          </form>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" color="success">
            Xem trước
          </Button>
        </DialogActions>
      </BootstrapDialog>
    </div>
  );
};
export default PrivateReportForm;

{
  /* <Input />
            <BasicSelect />
            <BasicSelect /> */
}
