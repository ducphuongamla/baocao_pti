import React from 'react';
import styles from './styles.module.scss';

export default function OpenLockedAccount({ setOpenLockedAccount }) {
  return (
    <div className={styles.wrapper}>
      <div className="container">
        <div className={styles.form}>
          <form>
            <div className={styles.title}>
              <p>Mở tài khoản bị khóa</p>
              <span
                onClick={() => setOpenLockedAccount(false)}
                style={{
                  cursor: "pointer",
                  transform: "translateY(1rem) scale(1.5)",
                  color: "#333",
                  fontWeight: "600",
                }}
              >
                X
              </span>
            </div>

            <div className={styles.content}>
              <div className={styles.block}>
                <div className={styles.item}>
                  <label>Hệ thống</label>
                  <select></select>
                </div>

                <div className={styles.item}>
                  <label>Đơn vị</label>
                  <select></select>
                </div>

                <div className={styles.item}>
                  <label>Mã NSD</label>
                  <input placeholder="password" />
                </div>
              </div>

              <h3>Cách thức để bạn có thể mở user đang bị khóa:</h3>
              <ul>
                <li>
                  <span>Chọn hệ thống cần mở khóa:</span>
                  <ul>
                    <li>
                      1. Mặc định là mở khóa hệ thống nội bộ:phần mềm nghiệp vụ,
                      hệ thống báo cáo.
                    </li>
                    <li>2. Hệ thống kênh bán.</li>
                  </ul>
                </li>
                <li>Chọn đơn vị bạn đang sử dụng.</li>
                <li>Gõ Người sử dụng đang sử dụng.</li>
                <li>
                  Bấm{" "}
                  <span style={{ color: "#ff0000", fontWeight: 700 }}>
                    Gửi thông tin.
                  </span>
                </li>
                <li>
                  <span style={{ fontWeight: 700 }}>
                    Quá trình xử lý tiếp theo sẽ như sau:
                  </span>
                  <ul>
                    <li>
                      1. Nếu Mã Người sử dụng trên chưa được đăng ký email thì
                      hệ thống sẽ mở màn hình cho phép bạn khai báo email.
                    </li>
                    <li>
                      2. Nếu Mã Người sử dụng trên đã được đăng ký email thì hệ
                      thống sẽ gửi 01 email chứa đường LINK active tài khoản.
                    </li>
                  </ul>
                </li>
              </ul>
              <p style={{ fontWeight: 700 }}>
                Bạn hãy làm theo hướng dẫn của email đó.
              </p>
            </div>

            <div className={styles.footer}>
              <button>gửi thông tin</button>
              <div onClick={() => setOpenLockedAccount(false)}>close</div>
            </div>
          </form>
        </div>
      </div>
      <div
        className={styles.cover}
        onClick={() => setOpenLockedAccount(false)}
      ></div>
    </div>
  );
}
