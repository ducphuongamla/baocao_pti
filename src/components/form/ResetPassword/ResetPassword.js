import React from 'react';
import styles from './styles.module.scss';

export default function ResetPassword({ setOpenFormResetPass }) {
  return (
    <div className={styles.wrapper}>
      <div className="container">
        <div className={styles.form}>
          <form>
            <div className={styles.title}>
              <p>Reset mật khẩu tài khoản</p>
              <span
                onClick={() => setOpenFormResetPass(false)}
                style={{
                  cursor: "pointer",
                  transform: "translateY(1rem) scale(1.5)",
                  color: "#333",
                  fontWeight: "600",
                }}
              >
                X
              </span>
            </div>

            <div className={styles.content}>
              <div className={styles.block}>
                <div className={styles.item}>
                  <label>Đơn vị</label>
                  <select></select>
                </div>

                <div className={styles.item}>
                  <label>Email</label>
                  <input placeholder="Email, ví dụ: ****@pti.com.vn" />
                </div>
              </div>

              <h3>Cách thức để bạn có thể Reset tài khoản:</h3>
              <ul>
                <li>Chọn đơn vị bạn đang sử dụng</li>
                <li>Gõ Email đăng ký với người sử dụng</li>
                <li>
                  Bấm{" "}
                  <span style={{ color: "#ff0000", fontWeight: 700 }}>
                    Quên mật khẩu
                  </span>
                </li>
                <li>
                  <span style={{ fontWeight: 700 }}>
                    Quá trình xử lý tiếp theo sẽ như sau:
                  </span>
                  <ul>
                    <li>
                      1. Nếu Mã Người sử dụng trên chưa được đăng ký email thì
                      hệ thống sẽ mở màn hình cho phép bạn khai báo email
                    </li>
                    <li>
                      2. Nếu Mã Người sử dụng trên đã được đăng ký email thì hệ
                      thống sẽ gửi 01 email mật khẩu đã được Reset.
                    </li>
                  </ul>
                </li>
              </ul>
            </div>

            <div className={styles.footer}>
              <button>quên mật khẩu</button>
              <div onClick={() => setOpenFormResetPass(false)}>close</div>
            </div>
          </form>
        </div>
      </div>
      <div
        className={styles.cover}
        onClick={() => setOpenFormResetPass(false)}
      ></div>
    </div>
  );
}
