import { configureStore } from "@reduxjs/toolkit";
import login from "../slice/loginSlice";
import insurance from '../slice/insuranceSlice';
import privateReport from "../slice/reportPrivateSlice";

const rootReducer = {
  login: login,
  insurance: insurance,
  privateReport: privateReport,
};

const store = configureStore({
  reducer: rootReducer,
});

export default store;
