
const login = "/Đăng-nhập-hệ-thống.htm";

const insurance = "/Bảo-hiểm/Bảo-hiểm.htm";

const ketoan = {
  vattu: "/Kế-toán/Vật-tư.htm",
  baohiem: "/Kế-toán/Bảo-hiểm.htm",
  ketoan: "/Kế-toán/Kế-toán.htm",
  congno: "/Kế-toán/Công-nợ.htm",
  tiente: "/Kế-toán/Tiền-tệ.htm",
  thueVAT: "/Kế-toán/Thuế-VAT.htm",
  taisan: "/Kế-toán/Tài-sản.htm",
  phanbo: "/Kế-toán/Phân-bổ.htm",
};

const PrivateReport = '/Báo-cáo-riêng/Báo-cáo-hiệp-hội-bảo-hiểm.htm';

const Tracuu = {
  Ks_TLBT: "/Tra-cứu/Kiểm-soát-tỷ-lệ-bồi-thường.htm",
  HDAC: "/Tra-cứu/Hóa-đơn-ấn-chỉ.htm",
  HT_KPI: {
    SL_KPI: "/Tra-cứu/Số-liệu-KPI.htm",
    KPI_BTV: "/Tra-cứu/KPI-Bồi-thường-viên.htm",
    TS_KPI: "/Tra-cứu/Tham-số-KPI.htm",
    TH_KPI: "/Tra-cứu/Tổng-hợp-KPI-Bồi-thường-viên.htm",
    TH_KPI_2020: "/Tra-cứu/Tổng-hợp-KPI-2020.htm",
    SL_KPI_2020: "/Tra-cứu/Số-liệu-KPI-2020.htm",
  },
};




export { login, insurance, ketoan, PrivateReport, Tracuu };
