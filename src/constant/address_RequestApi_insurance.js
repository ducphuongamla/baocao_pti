import DOIMAIN from "./doimain";

export const GET_API_FIRST = DOIMAIN + "api/BAOCAO/F_KD";

export const GET_API_PNHOM = DOIMAIN + "api/MA/BAOCAO/F_MA_PNHOM_LKE";

export const GET_API_MABAOCAO = DOIMAIN + "api/MA/BAOCAO/F_MA_LKE";

export const GET_API_MABAOCAO_CT = DOIMAIN + "api/MA/BAOCAO/F_MA_CT_LKE";

export const GET_API_MA_DK_BC_DK = DOIMAIN + "api/BAOCAO/F_MA_DK_BC_DK";

export const GET_API_XLBC_NH = DOIMAIN + "api/BAOCAO/F_XLBC_NH";