import React from "react";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";
import { loginApi } from "../APIRequest/loginApi";



export const check_email = createAsyncThunk('login/checkmail', async (email) => {
    const response = await loginApi.login_check_email(email);
    return response
})

export const check_select= createAsyncThunk("login/select", async (payload) => {
  const response = await loginApi.login_select(payload);
  return response;
});


export const check_login = createAsyncThunk("login/account", async (payload) => {
  const response = await loginApi.login(payload);
  return response;
});


const loginSlice = createSlice({
  name: "login",
  initialState: {
    loading: false,
    current: {a:'a'},
    select: '',
    token: '',
  },
  reducers: {},
  extraReducers: {
    [check_email.fulfilled]: (state, action) => {
      state.current = action.payload;
    },

    [check_select.fulfilled]: (state, action) => {
      state.select = action.payload;
    },

    [check_login.fulfilled]: (state, action) => {
      state.token = action.payload.data
    }
  },
});

const { reducer: login } = loginSlice;
export default login;