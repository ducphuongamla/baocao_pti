import { insuranceApi } from "../APIRequest/insuranceApi";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";

export const InitView = createAsyncThunk("report/kd", async (payload) => {
  const response = await insuranceApi.Register(payload);
  return response;
});

export const ListSubGroup = createAsyncThunk(
  "report/group",
  async (payload) => {
    const response = await insuranceApi.ListSubGroup(payload);
    return response;
  }
);
export const ListCode = createAsyncThunk("report/group", async (payload) => {
  const response = await insuranceApi.ListCode(payload);
  return response;
});
export const ListCodeTemplate = createAsyncThunk(
  "report/group/1",
  async (payload) => {
    const response = await insuranceApi.ListCodeTemplate(payload);
    return response;
  }
);
export const RenderController = createAsyncThunk(
  "report/group/2",
  async (payload) => {
    const response = await insuranceApi.ReportCondition(payload);
    return response;
  }
);
export const ReportExport = createAsyncThunk(
  "report/group/3",
  async (payload) => {
    const response = await insuranceApi.ReportExport(payload);
    return response;
  }
);

const insuranceSlice = createSlice({
  name: "insurance",
  initialState: {
    init: {},
    dataKD: null,
    dataTableLeft: null,
    dataTableRight: null,
    show: false,
    dataLabel: {},
    nhom: null,
    tenRp: null,
    templatePrint: null,
    dataController: null,
    dataDrop: null,
  },
  reducers: {
    visible: (state, action) => {
      state.show = action.payload;
    },
    hidden: (state, action) => {
      state.show = action.payload;
    },
    DataKD: (state, action) => {
      state.dataKD = action.payload;
    },

    printBC: (state, action) => {
      state.templatePrint = action.payload;
    },
    nhomBC: (state, action) => {
      state.nhom = action.payload;
    },
    TenRP: (state, action) => {
      state.tenRp = action.payload;
    },
    labelController: (state, action) => {
      state.dataLabel = action.payload;
    },
    requestController: (state, action) => {
      state.dataController = action.payload;
    },
    requestDrop: (state, action) => {
      state.dataDrop = action.payload;
    },

    resetDataTableRight: (state) => {
      state.dataTableRight = [];
    }
  },
  extraReducers: {
    [InitView.fulfilled]: (state, action) => {
      state.init = action.payload;
    },

    [ListCode.fulfilled]: (state, action) => {
      state.dataTableLeft = action.payload.data;
    },

    [ListCodeTemplate.fulfilled]: (state, action) => {
      state.dataTableRight = action.payload.data.lst_mau;
    },
  },
});

const { reducer: insurance, actions } = insuranceSlice;

export const {
  visible,
  hidden,
  DataKD,
  DataTableLeft,
  DataTableRight,
  requestController,
  requestDrop,
  printBC,
  nhomBC,
  TenRP,
  labelController,
  resetDataTableRight,
} = actions;

export default insurance;
