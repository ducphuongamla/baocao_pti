import React from "react";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";
import { privateReportApi } from "../APIRequest/privateReportApi";

export const get_api_init = createAsyncThunk(
  "privateRP/kd",
  async (data) => {
    const response = await privateReportApi.getApiInit(data);
    return response;
  }
);


const privateReportSlice = createSlice({
  name: "privateReport",
  initialState: {
    loading: false,
    dataInit: [],
    dataReview: []
  },
  reducers: {},
  extraReducers: {
    [get_api_init.fulfilled]: (state, action) => {
      state.dataInit = action.payload.data;
    },
  },
});

const { reducer: privateReport } = privateReportSlice;
export default privateReport;
