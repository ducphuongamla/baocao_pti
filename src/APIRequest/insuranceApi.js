
import * as URL from '../constant/address_RequestApi_insurance';
import axiosClient from '../untils/axiosClient';


export const insuranceApi = {
  Register(data) {
    const url = URL.GET_API_FIRST;
    return axiosClient.post(url, data);
  },
  ListSubGroup(data) {
    const url = URL.GET_API_PNHOM;
    return axiosClient.post(url, data);
  },
  ListCode(data) {
    const url = URL.GET_API_MABAOCAO;
    return axiosClient.post(url, data);
  },
  ListCodeTemplate(data) {
    const url = URL.GET_API_MABAOCAO_CT;
    return axiosClient.post(url, data);
  },
  ReportCondition(data) {
    const url = URL.GET_API_MA_DK_BC_DK;
    return axiosClient.post(url, data);
  },
  ReportExport(data) {
    const url = URL.GET_API_XLBC_NH;
    return axiosClient.post(url, data);
  },
  // ViewProcessKD(data) {
  //   const url = API_URL.DOMAINTEST + ReportApiUrl.F_XLBC_LKE;
  //   return axiosClient.post(url, data);
  // },
  // ViewProcessDataGrid(data) {
  //   const url = API_URL.DOMAINTEST + ReportApiUrl.F_XLBC_CT;
  //   return axiosClient.post(url, data);
  // },
  // ViewProcessExport(data) {
  //   const url = API_URL.DOMAINTEST + ReportApiUrl.ExecuteFromApplication;
  //   return axiosClient.post(url, data);
  // },
};
