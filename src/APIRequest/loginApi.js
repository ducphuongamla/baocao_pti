import axiosClient from "../untils/axiosClient";
import DOIMAIN from "../constant/doimain.js";
import * as LOGIN from "../constant/address_RequestApi_login";

export const loginApi = {
  login_check_email(email) {
    const url = DOIMAIN + LOGIN.LOGIN_CHECK_EMAIL;
    return axiosClient.post(url, {
      data: JSON.stringify({ dns: "", email: email, loai: "D" }),
    });
  },

  login(payload) {
    const url = DOIMAIN + LOGIN.LOGIN_ACCOUNT;
    return axiosClient.post(url, {
      data: JSON.stringify({
        md: "BH",
        dns: payload.maDonvi,
        llogin: "C",
        ma_dvi: payload.maDonvi,
        email: payload.email,
        ma_nsd: payload.maNsd,
        pas: payload.password,
        bctd: "false",
        semail: "false",
      }),
    });
  },

  login_select(payload) {
    const url = DOIMAIN + LOGIN.LOGIN_SELECT;
    return axiosClient.post(url, {
      data: JSON.stringify({
        dns: payload.dns ?? "00",
        email: payload.email,
        loai: "N",
      }),
    });
  },

};


