import axiosClient from "../untils/axiosClient";
import * as URL from "../constant/address_RequestApi_PrivateReport";




export const privateReportApi = {


    getApiInit(data) {
        const url = URL.GET_API_INIT;
        return axiosClient.post(url, data);
    },

    getApiReviewReport(data) {
        const url = URL.GET_API_REVIEW_BC;
        return axiosClient.post(url, data);
    }

};